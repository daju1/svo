#ifndef _SYS_TIME_H_
#define _SYS_TIME_H_
#include <time.h>
	
typedef long long          int64_t;
#define HAVE_GETSYSTEMTIMEASFILETIME 1
#include <windows.h>

#ifdef	__cplusplus
extern "C" {
#endif

#if ! defined _WINSOCKAPI_ || defined ROS_PLATFORM_WINSOCKAPI_DEFINED
#ifndef _TIMEVAL_DEFINED /* also in winsock[2].h */
#define _TIMEVAL_DEFINED
struct timeval {
  long tv_sec;
  long tv_usec;
};
#define timerisset(tvp)	 ((tvp)->tv_sec || (tvp)->tv_usec)
#define timercmp(tvp, uvp, cmp) \
	(((tvp)->tv_sec != (uvp)->tv_sec) ? \
	((tvp)->tv_sec cmp (uvp)->tv_sec) : \
	((tvp)->tv_usec cmp (uvp)->tv_usec))
#define timerclear(tvp)	 (tvp)->tv_sec = (tvp)->tv_usec = 0
#endif /* _TIMEVAL_DEFINED */
#endif

/* Provided for compatibility with code that assumes that
   the presence of gettimeofday function implies a definition
   of struct timezone. */
struct timezone
{
  int tz_minuteswest; /* of Greenwich */
  int tz_dsttime;     /* type of dst correction to apply */
};

/*
   Implementation as per:
   The Open Group Base Specifications, Issue 6
   IEEE Std 1003.1, 2004 Edition

   The timezone pointer arg is ignored.  Errors are ignored.
*/

int64_t inline static av_gettime(void)
{
#if HAVE_GETTIMEOFDAY
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return (int64_t)tv.tv_sec * 1000000 + tv.tv_usec;
#elif HAVE_GETSYSTEMTIMEASFILETIME
    FILETIME ft;
    int64_t t;
    GetSystemTimeAsFileTime(&ft);
    t = (int64_t)ft.dwHighDateTime << 32 | ft.dwLowDateTime;
    return t / 10 - 11644473600000000; /* Jan 1, 1601 */
#else
    return -1;
#endif
}
int inline static __cdecl gettimeofday(struct timeval * /*__restrict__*/ tv,
			 void * /*__restrict__*/ /*	tzp (unused) */)
{
	int64_t av_time = av_gettime();
	tv->tv_sec = av_time / 1000000;
	tv->tv_usec = av_time % 1000000;
	return 0;
}





#ifdef	__cplusplus
}
#endif


#endif /* _SYS_TIME_H_ */
