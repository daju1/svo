namespace std
{
bool static inline isnan(double var)
{
    volatile double d = var;
    return d != d;
}
}