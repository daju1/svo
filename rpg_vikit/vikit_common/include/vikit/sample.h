#ifndef VIKIT_SAMPLE_H_
#define VIKIT_SAMPLE_H_

#include <boost/random.hpp>
#include <boost/chrono.hpp>
#include <vikit/common.h>

namespace vk {

class Sample
{
public:
  static void setTimeBasedSeed();
  static int uniform(int from, int to);
  static double uniform();
  static double gaussian(double sigma);
  static boost::random::ranlux24 gen_real;
  static boost::random::mt19937 gen_int;
};

boost::random::ranlux24 Sample::gen_real;
boost::random::mt19937 Sample::gen_int;

void Sample::setTimeBasedSeed()
{
  unsigned seed = boost::chrono::system_clock::now().time_since_epoch().count();
  gen_real = boost::random::ranlux24(seed);
  //gen_int = std::mt19937(seed);
  gen_int = boost::random::mt19937(boost::chrono::system_clock::now().time_since_epoch().count());

}

int Sample::uniform(int from, int to)
{
  boost::random::uniform_int_distribution<int> distribution(from, to);
  return distribution(gen_int);
}

double Sample::uniform()
{
  boost::random::uniform_real_distribution<double> distribution(0.0, 1.0);
  return distribution(gen_real);
}

double Sample::gaussian(double stddev)
{
  boost::random::normal_distribution<double> distribution(0.0, stddev);
  return distribution(gen_real);
}

} // namespace vk

#endif // VIKIT_SAMPLE_H_
