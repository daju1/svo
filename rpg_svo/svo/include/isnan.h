static inline bool isnan(double var)
{
    volatile double d = var;
    return d != d;
}
