// This file is part of SVO - Semi-direct Visual Odometry.
//
// Copyright (C) 2014 Christian Forster <forster at ifi dot uzh dot ch>
// (Robotics and Perception Group, University of Zurich, Switzerland).
//
// SVO is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or any later version.
//
// SVO is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <svo/config.h>
#include <svo/frame_handler_mono.h>
#include <svo/map.h>
#include <svo/frame.h>
#include <svo/feature.h>
#include <svo/point.h>
#include <vector>
#include <string>
#include <vikit/math_utils.h>
#include <vikit/vision.h>
#include <vikit/abstract_camera.h>
#include <vikit/atan_camera.h>
#include <vikit/pinhole_camera.h>
#include <vikit/omni_camera.h>
#include <opencv2/opencv.hpp>
#define USE_VIZ 1 
#if USE_VIZ
#include <opencv2/viz.hpp>
#endif
#include <sophus/se3.h>
#include <iostream>
#include "test_utils.h"

namespace svo {




class BenchmarkNode
{
  vk::AbstractCamera* cam_;
  svo::FrameHandlerMono* vo_;
  cv::VideoCapture cap_;

#if USE_VIZ
  cv::viz::Viz3d* viz_;
  std::vector<cv::Affine3d> path;
  int getFeatures(cv::Mat* pcloud);
#endif
  cv::Vec3d rot_vec_;
  cv::Vec3d translation_;

public:
  BenchmarkNode();
  ~BenchmarkNode();
#if USE_VIZ
  void initViz();
  void updateViz();
#endif
#if !USE_CAM
  void runFromFolder();
#else
  void runFromWebCam();
#endif
};;

BenchmarkNode::BenchmarkNode()
{
#if USE_CAM
  int ndevice = 9;
printf("Enter device number\n");
  std::cin >> ndevice;
  while(!cap_.open(ndevice))
  {
    --ndevice;
    if (ndevice < -1)
      return;
  }
  int width = cap_.get(CV_CAP_PROP_FRAME_WIDTH);
  int height = cap_.get(CV_CAP_PROP_FRAME_HEIGHT);
  printf("ndevice=%d\n", ndevice);
  printf("width=%d, height=%d\n", width, height);



#else
#if USE_KITTY
  int width = 1241; // 1232
  int height = 376; // 368

#else
#if USE_PEOPLE_COUNTER
  //int width = 1280;
  //int height = 720;

  if(!cap_.open("E:/_C++/budnikov/test_work_copy/babka.avi"))
  {
      return;
  }
  int width = cap_.get(CV_CAP_PROP_FRAME_WIDTH);
  int height = cap_.get(CV_CAP_PROP_FRAME_HEIGHT);
  printf("width=%d, height=%d\n", width, height);

#else
  int width = 752;
  int height = 480;
#endif
#endif
#endif

  int n_levels = max(Config::nPyrLevels(), Config::kltMaxLevel()+1);
  int m = pow(2.0, n_levels - 1);

  width /= m; width *= m;
  height /= m; height *= m;


#if !USE_CAM
#if USE_KITTY
  cam_ = new vk::PinholeCamera(width, height, 718.856, 718.856, 607.1928, 185.2157);
#else
#if USE_PEOPLE_COUNTER
  cam_ = new vk::PinholeCamera(width, height, 718.856, 718.856, 607.1928, 185.2157);
#else
  cam_ = new vk::PinholeCamera(width, height, 315.5, 315.5, 376.0, 240.0);
#endif
#endif
#else
  //cam_ = new vk::OmniCamera("D:\\svo\\calib_results3_soni.txt");
  cam_ = new vk::OmniCamera("calib_results.txt");
#endif
#if USE_VIZ
  viz_ = new cv::viz::Viz3d("abc");
  initViz();
#endif
  vo_ = new svo::FrameHandlerMono(cam_);
  vo_->start();
}

BenchmarkNode::~BenchmarkNode()
{
  delete vo_;
  delete cam_;
}
#if USE_VIZ
void BenchmarkNode::initViz()
{
    viz_->setBackgroundMeshLab();
    viz_->showWidget("coo", cv::viz::WCoordinateSystem(1));

    /// Construct a cube widget
    cv::viz::WCube cube_widget(cv::Point3d(0.5, 0.5, 0.0), cv::Point3d(0.0, 0.0, -0.5), true, cv::viz::Color::blue());
    cube_widget.setRenderingProperty(cv::viz::LINE_WIDTH, 4.0);

    /// Display widget (update if already displayed)
    viz_->showWidget("Cube Widget", cube_widget);

    /// Rodrigues vector
    rot_vec_ = cv::Vec3d::all(0);
    translation_ = cv::Vec3d::all(0);
    viz_->spinOnce();
}

void fill_cloud(cv::Mat* pcloud, int i_cloud_point, Eigen::Vector3d& f)
{
   if (pcloud)
   {
      if ((*pcloud).depth() == CV_32F){
         cv::Vec3f *fdata = (*pcloud).ptr<cv::Vec3f>(i_cloud_point);
         *fdata = cv::Vec3f(f.data()[0], f.data()[1], f.data()[2]);
      }

      if ((*pcloud).depth() == CV_64F){
         cv::Vec3d *ddata = (*pcloud).ptr<cv::Vec3d>(i_cloud_point);
         *ddata = cv::Vec3d(f.data()[0], f.data()[1], f.data()[2]);
      }
   }
}


int BenchmarkNode::getFeatures(cv::Mat* pcloud)
{
    int i_cloud_point = 0;

    list< FramePtr >::const_iterator kf     = vo_->map_.keyframes_.begin();
    list< FramePtr >::const_iterator kf_end = vo_->map_.keyframes_.end();
    for (; kf != kf_end; ++kf)
    {
      vector<Feature*>::const_iterator keypoint     = (*kf)->key_pts_.begin();
      vector<Feature*>::const_iterator keypoint_end = (*kf)->key_pts_.end();
      for (; keypoint != keypoint_end; ++keypoint)
      {
        if((*keypoint) == 0)
          continue;

        if ((*keypoint)->point == 0)
           continue;

        fill_cloud(pcloud, i_cloud_point++, (*keypoint)->point->pos_);
      }

      list<Feature*>::const_iterator feature_point     = (*kf)->fts_.begin();
      list<Feature*>::const_iterator feature_point_end = (*kf)->fts_.end();
      for (; feature_point != feature_point_end; ++feature_point)
      {
        if((*feature_point) == 0)
          continue;

        if ((*feature_point)->point == 0)
           continue;

        fill_cloud(pcloud, i_cloud_point++, (*feature_point)->point->pos_);
      }
    }

    return i_cloud_point;
}

void BenchmarkNode::updateViz()
{
    SE3 se3 = vo_->lastFrame()->T_f_w_.inverse();
    // access the pose of the camera via vo_->lastFrame()->T_f_w_.
    const Eigen::VectorBlock<const Eigen::QuaternionBase<Eigen::Quaternion<double, DontAlign> >::Coefficients, 3> vo_rot_vec = se3.unit_quaternion().vec();
    rot_vec_[0] = vo_rot_vec[0];
    rot_vec_[1] = vo_rot_vec[1];
    rot_vec_[2] = vo_rot_vec[2];

    const Eigen::Vector3d vo_translation = se3.translation();
    translation_[0] = vo_translation[0];
    translation_[1] = vo_translation[1];
    translation_[2] = vo_translation[2];

    /// Construct pose
    cv::Affine3d pose(rot_vec_, translation_);
    path.push_back(pose);


    int cloud_size = getFeatures(NULL);
    if (cloud_size)
    {
       // ������ ����� � �����. ������� � ����� CV_32FC3
       cv::Mat cloud(cv::Size(1, cloud_size), CV_32FC3);
       getFeatures(&cloud);

       // ������� ������ ������ ��� ������ � ��������� ��� ���������� �������
       // cv::Mat colors(cloud.size(), CV_8UC3);

       viz_->showWidget("cloud", cv::viz::WCloud(cloud, cv::viz::Color::white()));
    }


    viz_->showWidget("sub3", cv::viz::WTrajectory(cv::Mat(path), cv::viz::WTrajectory::BOTH, 0.2, cv::viz::Color::brown()), path.front().inv());

    viz_->setWidgetPose("Cube Widget", pose);
    viz_->spinOnce(1, true);
}
#endif

#if !USE_CAM
#if !USE_KITTY
#if USE_PEOPLE_COUNTER
void BenchmarkNode::runFromFolder()
{


  for(int img_id = 2; ; ++img_id)
  {
    cv::Mat colorMat;
  cap_ >> colorMat;
  int n_try = 0;
    while(colorMat.empty() && n_try < 25)
  {
    cap_ >> colorMat;
    Sleep(100);
    n_try++;
  }

  if (colorMat.empty())
  {
    printf("colorMat.empty()\n");
    break;
  }

  cv::imshow("image", colorMat);
  int wait_key = cv::waitKey(1);
//  printf("wait_key=%d\n", wait_key);
  if( wait_key == 27 ){
    break; // stop capturing by pressing ESC 
  }

  if (115 == wait_key)
  {
    static int img_number = 1;
    std::stringstream ss;

    ss << "_img_" << img_number << ".bmp";

    cv::imwrite(ss.str(), colorMat);

    

    img_number++;
  }

  cv::Mat greyMat;
    cv::cvtColor(colorMat, greyMat, CV_BGR2GRAY);

    // process frame
    vo_->addImage(greyMat, 0.01*img_id);

    // display tracking quality
    if(vo_->lastFrame() != NULL)
    {
      std::cout << "Frame-Id: " << vo_->lastFrame()->id_ << " \t"
                  << "#Features: " << vo_->lastNumObservations() << " \t"
                  << "Proc. Time: " << vo_->lastProcessingTime()*1000 << "ms \n"
          << "translation" << endl << vo_->lastFrame()->T_f_w_.translation() << "\n"
          << "rotation_matrix" << endl << vo_->lastFrame()->T_f_w_.rotation_matrix() << "\n"
          << "unit_quaternion().vec" << endl << vo_->lastFrame()->T_f_w_.unit_quaternion().vec() << "\n"
          ;

      // access the pose of the camera via vo_->lastFrame()->T_f_w_.
    updateViz();
    }
  }
}
#else
void BenchmarkNode::runFromFolder()
{
  for(int img_id = 2; img_id < 188; ++img_id)
  {
    // load image
    std::stringstream ss;
    ss 
      //<< svo::test_utils::getDatasetDir() << "/"
      << "D:/svo/dataset/sin2_tex2_h1_v8_d/img/frame_"
       << std::setw( 6 ) << std::setfill( '0' ) << img_id << "_0.png";
    if(img_id == 2)
      std::cout << "reading image " << ss.str() << std::endl;
    cv::Mat img(cv::imread(ss.str().c_str(), 0));
    assert(!img.empty());

    cv::imshow("image", img);
    if( cv::waitKey(1) == 27 ) break; // stop capturing by pressing ESC 

    // process frame
    vo_->addImage(img, 0.01*img_id);

    // display tracking quality
    if(vo_->lastFrame() != NULL)
    {
        std::cout << "Frame-Id: " << vo_->lastFrame()->id_ << " \t"
                  << "#Features: " << vo_->lastNumObservations() << " \t"
                  << "Proc. Time: " << vo_->lastProcessingTime()*1000 << "ms \n"
                  << vo_->lastFrame()->T_f_w_.translation() << "\n"
                  << vo_->lastFrame()->T_f_w_.rotation_matrix() << "\n"
                  << vo_->lastFrame()->T_f_w_.unit_quaternion().vec() << "\n"
                  ;

      // access the pose of the camera via vo_->lastFrame()->T_f_w_.
#if USE_VIZ
      updateViz();
#endif
    }
  }
}
#endif
#else // USE_KITTY

void BenchmarkNode::runFromFolder()
{
	for (int img_id = 0; img_id <= 4540; ++img_id)
	{
		// load image
		std::stringstream ss;
		ss
			//<< svo::test_utils::getDatasetDir() << "/"
			<< "E:/kitti/data_odometry_gray/dataset/sequences/00/image_0/"
			<< std::setw(6) << std::setfill('0') << img_id << ".png";
		if (img_id == 2)
			std::cout << "reading image " << ss.str() << std::endl;
		cv::Mat img(cv::imread(ss.str().c_str(), 0));
		assert(!img.empty());

		cv::Rect RectangleToSelect(5,4,1232, 368);


		cv::Mat im = img(RectangleToSelect);

		cv::imshow("image", im);
		if (cv::waitKey(1) == 27) break; // stop capturing by pressing ESC 

		// process frame
		vo_->addImage(im, 0.01*img_id);

		// display tracking quality
		if (vo_->lastFrame() != NULL)
		{
			std::cout << "Frame-Id: " << vo_->lastFrame()->id_ << " \t"
				<< "#Features: " << vo_->lastNumObservations() << " \t"
				<< "Proc. Time: " << vo_->lastProcessingTime() * 1000 << "ms \n"
				<< vo_->lastFrame()->T_f_w_.translation() << "\n"
				<< vo_->lastFrame()->T_f_w_.rotation_matrix() << "\n"
				<< vo_->lastFrame()->T_f_w_.unit_quaternion().vec() << "\n"
				;

			// access the pose of the camera via vo_->lastFrame()->T_f_w_.
#if USE_VIZ
			updateViz();
#endif
		}
	}
}
#endif
#else //USE_CAM
void BenchmarkNode::runFromWebCam()
{


  for(int img_id = 2; ; ++img_id)
  {
    cv::Mat colorMat;
	cap_ >> colorMat;
	int n_try = 0;
    while(colorMat.empty() && n_try < 25)
	{
		cap_ >> colorMat;
		Sleep(100);
		n_try++;
	}

	if (colorMat.empty())
	{
		printf("colorMat.empty()\n");
		break;
	}

	cv::imshow("image", colorMat);
	int wait_key = cv::waitKey(1);
//	printf("wait_key=%d\n", wait_key);
	if( wait_key == 27 ){
		break; // stop capturing by pressing ESC 
	}

	if (115 == wait_key)
	{
		static int img_number = 1;
		std::stringstream ss;

		ss << "_img_" << img_number << ".bmp";

		cv::imwrite(ss.str(), colorMat);

		

		img_number++;
	}

	cv::Mat greyMat;
    cv::cvtColor(colorMat, greyMat, CV_BGR2GRAY);

    // process frame
    vo_->addImage(greyMat, 0.01*img_id);

    // display tracking quality
    if(vo_->lastFrame() != NULL)
    {
    	std::cout << "Frame-Id: " << vo_->lastFrame()->id_ << " \t"
                  << "#Features: " << vo_->lastNumObservations() << " \t"
                  << "Proc. Time: " << vo_->lastProcessingTime()*1000 << "ms \n"
				  << "translation" << endl << vo_->lastFrame()->T_f_w_.translation() << "\n"
				  << "rotation_matrix" << endl << vo_->lastFrame()->T_f_w_.rotation_matrix() << "\n"
				  << "unit_quaternion().vec" << endl << vo_->lastFrame()->T_f_w_.unit_quaternion().vec() << "\n"
				  ;

    	// access the pose of the camera via vo_->lastFrame()->T_f_w_.
		updateViz();
    }
  }
}
#endif
} // namespace svo


#if USE_VIZ
void test_viz()
{
    //cv::Mat cloud = cv::viz::readCloud(get_dragon_ply_file_path());

    cv::viz::Viz3d viz("abc");
    viz.setBackgroundMeshLab();
    viz.showWidget("coo", cv::viz::WCoordinateSystem(1));
    //viz.showWidget("cloud", cv::viz::WPaintedCloud(cloud));

    //---->>>>> <to_test_in_future>
    //std::vector<cv::Affine3d> gt, es;
    //cv::viz::readTrajectory(gt, "d:/Datasets/trajs/gt%05d.xml");
    //cv::viz::readTrajectory(es, "d:/Datasets/trajs/es%05d.xml");
    //cv::Mat cloud = cv::viz::readCloud(get_dragon_ply_file_path());
    //---->>>>> </to_test_in_future>

    viz.spin();
}

void tutorial2()
{
    /// Create a window
    cv::viz::Viz3d myWindow("Coordinate Frame");

    /// Add coordinate axes
    myWindow.showWidget("Coordinate Widget", cv::viz::WCoordinateSystem());

    /// Add line to represent (1,1,1) axis
    cv::viz::WLine axis(cv::Point3f(-1.0, -1.0, -1.0), cv::Point3d(1.0, 1.0, 1.0));
    axis.setRenderingProperty(cv::viz::LINE_WIDTH, 4.0);
    myWindow.showWidget("Line Widget", axis);

    /// Construct a cube widget
    cv::viz::WCube cube_widget(cv::Point3d(0.5, 0.5, 0.0), cv::Point3d(0.0, 0.0, -0.5), true, cv::viz::Color::blue());
    cube_widget.setRenderingProperty(cv::viz::LINE_WIDTH, 4.0);

    /// Display widget (update if already displayed)
    myWindow.showWidget("Cube Widget", cube_widget);

    /// Rodrigues vector
    cv::Vec3d rot_vec = cv::Vec3d::all(0);
    double translation_phase = 0.0, translation = 0.0;
    while(!myWindow.wasStopped())
    {
        /* Rotation using rodrigues */
        /// Rotate around (1,1,1)
        rot_vec[0] += CV_PI * 0.01;
        rot_vec[1] += CV_PI * 0.01;
        rot_vec[2] += CV_PI * 0.01;

        /// Shift on (1,1,1)
        translation_phase += CV_PI * 0.01;
        translation = sin(translation_phase);

        /// Construct pose
        cv::Affine3d pose(rot_vec, cv::Vec3d(translation, translation, translation));

        myWindow.setWidgetPose("Cube Widget", pose);

        myWindow.spinOnce(1, true);
    }
}


void tutorial3(bool camera_pov)
{
    /// Create a window
    cv::viz::Viz3d myWindow("Coordinate Frame");

    /// Add coordinate axes
    myWindow.showWidget("Coordinate Widget", cv::viz::WCoordinateSystem());

    /// Let's assume camera has the following properties
    cv::Point3d cam_origin(3.0, 3.0, 3.0), cam_focal_point(3.0, 3.0, 2.0), cam_y_dir(-1.0, 0.0, 0.0);

    /// We can get the pose of the cam using makeCameraPose
    cv::Affine3d camera_pose = cv::viz::makeCameraPose(cam_origin, cam_focal_point, cam_y_dir);

    /// We can get the transformation matrix from camera coordinate system to global using
    /// - makeTransformToGlobal. We need the axes of the camera
    cv::Affine3d transform = cv::viz::makeTransformToGlobal(cv::Vec3d(0.0, -1.0, 0.0), cv::Vec3d(-1.0, 0.0, 0.0), cv::Vec3d(0.0, 0.0, -1.0), cam_origin);
#if 0
    /// Create a cloud widget.
    cv::Mat dragon_cloud = cv::viz::readCloud(get_dragon_ply_file_path());
    cv::viz::WCloud cloud_widget(dragon_cloud, cv::viz::Color::green());

    /// Pose of the widget in camera frame
    cv::Affine3d cloud_pose = cv::Affine3d().rotate(cv::Vec3d(0.0, CV_PI/2, 0.0)).rotate(cv::Vec3d(0.0, 0.0, CV_PI)).translate(cv::Vec3d(0.0, 0.0, 3.0));
    /// Pose of the widget in global frame
    cv::Affine3d cloud_pose_global = transform * cloud_pose;
#endif
    /// Visualize camera frame
    myWindow.showWidget("CPW_FRUSTUM", cv::viz::WCameraPosition(cv::Vec2f(0.889484f, 0.523599f)), camera_pose);
    if (!camera_pov)
        myWindow.showWidget("CPW", cv::viz::WCameraPosition(0.5), camera_pose);
#if 0
    /// Visualize widget
    myWindow.showWidget("bunny", cloud_widget, cloud_pose_global);
#endif
    /// Set the viewer pose to that of camera
    if (camera_pov)
        myWindow.setViewerPose(camera_pose);

    /// Start event loop.
    myWindow.spin();
}


#endif
int main(int argc, char** argv)
{
	//test_viz();
	//tutorial2();
	//tutorial3(true);
	//tutorial3(false);
  {
    svo::BenchmarkNode benchmark;
#if !USE_CAM
    benchmark.runFromFolder();
#else
    benchmark.runFromWebCam();
#endif
  }
  printf("BenchmarkNode finished.\n");
  return 0;
}

