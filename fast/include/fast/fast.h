#ifndef FAST_H
#define FAST_H

#ifdef _WIN32
#define __SSE2__ 1
#endif

#include <vector>
#if 0
  #ifdef fast_EXPORTS // we are building a shared lib/dll
    #define FAST_DECL __declspec(dllexport)
  #else // we are using shared lib/dll
    #define FAST_DECL __declspec(dllimport)
  #endif
#else
#define FAST_DECL
#endif

namespace fast
{

using ::std::vector;

struct FAST_DECL fast_xy
{
  short x, y;
  fast_xy(short x_, short y_) : x(x_), y(y_) {}
};

typedef unsigned char fast_byte;

/// SSE2 optimized version of the corner 10
void FAST_DECL fast_corner_detect_10_sse2(const fast_byte* img, int imgWidth, int imgHeight, int widthStep, short barrier, vector<fast_xy>& corners);      

/// plain C++ version of the corner 10
void FAST_DECL fast_corner_detect_10(const fast_byte* img, int imgWidth, int imgHeight, int widthStep, short barrier, vector<fast_xy>& corners); 

/// corner score 10
void FAST_DECL fast_corner_score_10(const fast_byte* img, const int img_stride, const vector<fast_xy>& corners, const int threshold, vector<int>& scores);

/// Nonmax Suppression on a 3x3 Window
void FAST_DECL fast_nonmax_3x3(const vector<fast_xy>& corners, const vector<int>& scores, vector<int>& nonmax_corners);

/// NEON optimized version of the corner 9
void FAST_DECL fast_corner_detect_9_neon(const fast_byte* img, int imgWidth, int imgHeight, int widthStep, short barrier, vector<fast_xy>& corners);     

} // namespace fast

#endif
